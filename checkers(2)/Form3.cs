﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace checkers_2_
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            back.FlatAppearance.MouseOverBackColor = Color.FromArgb(27, 255, 255, 255);
            back.FlatAppearance.MouseDownBackColor = Color.FromArgb(50, 255, 255, 255);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            int selectedIndex = comboBox1.SelectedIndex;


            string imagePath = string.Empty;
            switch (selectedIndex)
            {
                case 0:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\0_ua.png");
                    break;
                case 1:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\1_ua.png");
                    break;
                case 2:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\2_ua.png");
                    break;
                case 3:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\3_ua.png");
                    break;
                case 4:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\4_ua.png");
                    break;
                case 5:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\5_ua.png");
                    break;
                case 6:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\6_ua.png");
                    break;
                case 7:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\7_ua.png");
                    break;
                case 8:
                    imagePath = (@"C:\Users\User\source\repos\kursova(2)\checkers(2)\8_ua.png");
                    break;
            }


            if (!string.IsNullOrEmpty(imagePath))
            {
                this.BackgroundImage = Image.FromFile(imagePath);
            }
        }

        private void back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.ShowDialog();
            this.Close();
        }
    }
}
