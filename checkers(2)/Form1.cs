using checkers_2_.Properties;
using System.IO;
using System.Media;

namespace checkers_2_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            play.FlatAppearance.MouseOverBackColor = Color.FromArgb(27, 255, 255, 255);
            play.FlatAppearance.MouseDownBackColor = Color.FromArgb(50,255,255,255);

            howPlay.FlatAppearance.MouseOverBackColor = Color.FromArgb(27, 255, 255, 255);
            howPlay.FlatAppearance.MouseDownBackColor = Color.FromArgb(50, 255, 255, 255);

            exit.FlatAppearance.MouseOverBackColor = Color.FromArgb(27, 255, 255, 255);
            exit.FlatAppearance.MouseDownBackColor = Color.FromArgb(50, 255, 255, 255);

            string filePath = "radiobutton.txt";
            if (File.Exists(filePath))
            {
                string value = File.ReadAllText(filePath);
                bool isUaChecked = bool.Parse(value);
                uaRadioButton.Checked = isUaChecked;
                engRadioButton.Checked = !isUaChecked;
            }
            
        }

        private void play_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            this.Hide();
            form2.ShowDialog();
            this.Close();
        }

        private void ua_CheckedChanged(object sender, EventArgs e)
        {
            if (uaRadioButton.Checked)
            {
                play.Text = "�����";
                howPlay.Text = "�� �����?";
                exit.Text = "�����";
                this.Text = "�����";
                string filePath = "radiobutton.txt";
                File.WriteAllText(filePath, true.ToString());

            }
        }

        private void eng_CheckedChanged(object sender, EventArgs e)
        {
            if (engRadioButton.Checked)
            {
                play.Text = "Play";
                howPlay.Text = "How to play ?";
                exit.Text = "Exit";
                this.Text = "Checkers";
                string filePath = "radiobutton.txt";
                File.WriteAllText(filePath, false.ToString());
            }
        }

        private void howPlay_Click(object sender, EventArgs e)
        {
            if (uaRadioButton.Checked)
            {
                Form3 form3 = new Form3();
                this.Hide();
                form3.ShowDialog();
                this.Close();
            }
            if (engRadioButton.Checked)
            {
                Form4 form4 = new Form4();
                this.Hide();
                form4.ShowDialog();
                this.Close();
            }
        }
        SoundPlayer backgroundMusicPlayer = new SoundPlayer(@"C:\Users\User\source\repos\kursova(2)\checkers(2)\L.wav");
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            
            if (checkBox1.Checked == true)
            {
                backgroundMusicPlayer.PlayLooping();
            }
            if (checkBox1.Checked == false)
            {
                backgroundMusicPlayer.Stop();
            }
        }
    }
}