﻿namespace checkers_2_
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.back = new System.Windows.Forms.Button();
            this.res = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ww = new System.Windows.Forms.Label();
            this.bw = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.uaRadioButton = new System.Windows.Forms.RadioButton();
            this.engRadioButton = new System.Windows.Forms.RadioButton();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.Transparent;
            this.back.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.back.FlatAppearance.BorderSize = 0;
            this.back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.back.Location = new System.Drawing.Point(802, 654);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(311, 105);
            this.back.TabIndex = 0;
            this.back.Text = "Повернутися у меню";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // res
            // 
            this.res.BackColor = System.Drawing.Color.Transparent;
            this.res.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.res.FlatAppearance.BorderSize = 0;
            this.res.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.res.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.res.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.res.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.res.Location = new System.Drawing.Point(802, 543);
            this.res.Name = "res";
            this.res.Size = new System.Drawing.Size(311, 105);
            this.res.TabIndex = 1;
            this.res.Text = "Перезапустити гру";
            this.res.UseVisualStyleBackColor = false;
            this.res.Click += new System.EventHandler(this.res_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(873, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 54);
            this.label1.TabIndex = 2;
            this.label1.Text = "Рахунок";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ww
            // 
            this.ww.AutoSize = true;
            this.ww.BackColor = System.Drawing.Color.Transparent;
            this.ww.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ww.Location = new System.Drawing.Point(802, 163);
            this.ww.Name = "ww";
            this.ww.Size = new System.Drawing.Size(44, 52);
            this.ww.TabIndex = 3;
            this.ww.Text = "0";
            // 
            // bw
            // 
            this.bw.AutoSize = true;
            this.bw.BackColor = System.Drawing.Color.Transparent;
            this.bw.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.bw.Location = new System.Drawing.Point(1051, 163);
            this.bw.Name = "bw";
            this.bw.Size = new System.Drawing.Size(44, 52);
            this.bw.TabIndex = 4;
            this.bw.Text = "0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(811, 98);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 52);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1026, 98);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(69, 52);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // uaRadioButton
            // 
            this.uaRadioButton.AutoSize = true;
            this.uaRadioButton.BackColor = System.Drawing.Color.Transparent;
            this.uaRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("uaRadioButton.Image")));
            this.uaRadioButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.uaRadioButton.Location = new System.Drawing.Point(873, 12);
            this.uaRadioButton.Name = "uaRadioButton";
            this.uaRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.uaRadioButton.Size = new System.Drawing.Size(62, 34);
            this.uaRadioButton.TabIndex = 7;
            this.uaRadioButton.UseVisualStyleBackColor = false;
            this.uaRadioButton.CheckedChanged += new System.EventHandler(this.uaRadioButton_CheckedChanged);
            // 
            // engRadioButton
            // 
            this.engRadioButton.AutoSize = true;
            this.engRadioButton.BackColor = System.Drawing.Color.Transparent;
            this.engRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("engRadioButton.Image")));
            this.engRadioButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.engRadioButton.Location = new System.Drawing.Point(962, 12);
            this.engRadioButton.Name = "engRadioButton";
            this.engRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.engRadioButton.Size = new System.Drawing.Size(58, 34);
            this.engRadioButton.TabIndex = 8;
            this.engRadioButton.UseVisualStyleBackColor = false;
            this.engRadioButton.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.Transparent;
            this.checkBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("checkBox1.BackgroundImage")));
            this.checkBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(1051, 18);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(71, 24);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "          ";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Location = new System.Drawing.Point(873, 308);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(194, 229);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1107, 798);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.engRadioButton);
            this.Controls.Add(this.uaRadioButton);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bw);
            this.Controls.Add(this.ww);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.res);
            this.Controls.Add(this.back);
            this.Controls.Add(this.pictureBox3);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1125, 845);
            this.MinimumSize = new System.Drawing.Size(1125, 845);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Шашки";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button back;
        private Button res;
        private Label label1;
        private Label ww;
        private Label bw;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private RadioButton uaRadioButton;
        private RadioButton engRadioButton;
        private CheckBox checkBox1;
        private PictureBox pictureBox3;
    }
}