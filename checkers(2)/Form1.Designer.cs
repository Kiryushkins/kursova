﻿namespace checkers_2_
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.play = new System.Windows.Forms.Button();
            this.howPlay = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.uaRadioButton = new System.Windows.Forms.RadioButton();
            this.engRadioButton = new System.Windows.Forms.RadioButton();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // play
            // 
            this.play.BackColor = System.Drawing.Color.Transparent;
            this.play.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.play.FlatAppearance.BorderSize = 0;
            this.play.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.play.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.play, "play");
            this.play.ForeColor = System.Drawing.Color.Black;
            this.play.Name = "play";
            this.play.UseVisualStyleBackColor = false;
            this.play.Click += new System.EventHandler(this.play_Click);
            // 
            // howPlay
            // 
            this.howPlay.BackColor = System.Drawing.Color.Transparent;
            this.howPlay.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.howPlay.FlatAppearance.BorderSize = 0;
            this.howPlay.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.howPlay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            resources.ApplyResources(this.howPlay, "howPlay");
            this.howPlay.ForeColor = System.Drawing.Color.Black;
            this.howPlay.Name = "howPlay";
            this.howPlay.UseVisualStyleBackColor = false;
            this.howPlay.Click += new System.EventHandler(this.howPlay_Click);
            // 
            // exit
            // 
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.exit.FlatAppearance.BorderSize = 0;
            this.exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            resources.ApplyResources(this.exit, "exit");
            this.exit.ForeColor = System.Drawing.Color.Black;
            this.exit.Name = "exit";
            this.exit.UseVisualStyleBackColor = false;
            this.exit.Click += new System.EventHandler(this.button4_Click);
            // 
            // uaRadioButton
            // 
            resources.ApplyResources(this.uaRadioButton, "uaRadioButton");
            this.uaRadioButton.BackColor = System.Drawing.Color.Transparent;
            this.uaRadioButton.Name = "uaRadioButton";
            this.uaRadioButton.UseVisualStyleBackColor = false;
            this.uaRadioButton.CheckedChanged += new System.EventHandler(this.ua_CheckedChanged);
            // 
            // engRadioButton
            // 
            resources.ApplyResources(this.engRadioButton, "engRadioButton");
            this.engRadioButton.BackColor = System.Drawing.Color.Transparent;
            this.engRadioButton.Name = "engRadioButton";
            this.engRadioButton.UseVisualStyleBackColor = false;
            this.engRadioButton.CheckedChanged += new System.EventHandler(this.eng_CheckedChanged);
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuPopup;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.engRadioButton);
            this.Controls.Add(this.uaRadioButton);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.howPlay);
            this.Controls.Add(this.play);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button play;
        private Button howPlay;
        private Button exit;
        private RadioButton uaRadioButton;
        private RadioButton engRadioButton;
        private CheckBox checkBox1;
        private PictureBox pictureBox1;
    }
}